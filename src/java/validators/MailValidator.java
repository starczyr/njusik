package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author radek
 */
@FacesValidator("validators.mailValidator")
public class MailValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        System.err.println("starting validation");

        String inputValue = (String) value;
        if(!inputValue.matches("[a-zA-Z0-9._%+-]+@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,4}") && !inputValue.equals("")) {
            context.addMessage(component.getClientId(),
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "It doesn't look like real mail address!", ""));
            if(context.getPartialViewContext().isAjaxRequest() == false) {
                context.validationFailed();
            }
        } else if(inputValue.equals("")) {
            context.addMessage(component.getClientId(),
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "You have to enter Your mail!", ""));
            if(context.getPartialViewContext().isAjaxRequest() == false) {
                context.validationFailed();
            }
        }
        if(inputValue.length() > 50) {
            context.addMessage(component.getClientId(),
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Please, use shorter adress.", ""));
            if(context.getPartialViewContext().isAjaxRequest() == false) {
                context.validationFailed();
            }
        }
    }
}
