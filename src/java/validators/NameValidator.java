/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author radek
 */
@FacesValidator("validators.nameValidator")
public class NameValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String inputValue = (String) value;
        if(!inputValue.matches("^[a-zA-Z]*")) {
            context.addMessage(component.getClientId(), 
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "It doesn't look like real name!", ""));
            context.validationFailed();
        }
        if(inputValue.length() > 30) {
            context.addMessage(component.getClientId(), 
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Name can't be that long!", ""));
            context.validationFailed();
        }
    }
    
}
