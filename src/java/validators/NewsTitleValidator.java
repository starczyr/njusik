package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author radek
 */
@FacesValidator("validators.newsTitleValidator")
public class NewsTitleValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String inputValue = (String) value;
        if(inputValue.equals("")) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Aww!", "News got to have a title."));
            context.validationFailed();
        } else if(inputValue.length() > 60) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Aww!", "Please, make Your title shorter."));
            context.validationFailed();
        }
    }
    
}
