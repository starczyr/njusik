package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author radek
 */
@FacesValidator("validators.statusValidator")
public class StatusValidator implements Validator {
    

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String status = (String) value;
        if(status.length() > 300) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Aww.", "Please, make your status shorter."));
            context.validationFailed();
        }
    }
    
}
