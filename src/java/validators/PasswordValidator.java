package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author radek
 */
@FacesValidator("validators.passwordValidator")
public class PasswordValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String inputValue = (String) value;
        String[] invalidChars = {" ", "*"};

        if(inputValue.length() > 30) {
            context.addMessage(component.getClientId(),
                        new FacesMessage(FacesMessage.SEVERITY_WARN,
                        "Please, use shorter password.", ""));
                context.validationFailed();
        } else if(inputValue.length() < 5) {
            context.addMessage(component.getClientId(),
                        new FacesMessage(FacesMessage.SEVERITY_WARN,
                        "Please, use longer password.", ""));
                context.validationFailed();
        }

        for(String string : invalidChars) {
            if(inputValue.contains(string)) {
                context.addMessage(component.getClientId(),
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Password cannot contains '*' or ' '", ""));
                context.validationFailed();
            }
        }
        if(inputValue.equals("")) {
            context.addMessage(component.getClientId(),
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Password is obviously required.", ""));
            context.validationFailed();
        }
    }
}
