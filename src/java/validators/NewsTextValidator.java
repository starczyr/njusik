package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author radek
 */
@FacesValidator("validators.newsTextValidator")
public class NewsTextValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String inputValue = (String) value;
        if(inputValue.length() > 1000) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Ups!", "Please make Your news shorter."));
            context.validationFailed();

        } else if(inputValue.equals("")) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Ups!", "Please, don't send empty news."));
            context.validationFailed();
        }
    }
}