package reseter;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import user.UserAccount;
import user.UserAccountDAO;
import user.UserManager;

/**
 *
 * @author radek
 */
@Named(value = "reseterManager")
@SessionScoped
public class ReseterManager implements Serializable {

    @Inject
    UserManager userManager;
    private UserAccountDAO userAccountDAO = new UserAccountDAO();
    private String newPassword;
    private String newRepassword;

    public ReseterManager() {
    }

    public String changePassword(String adressToRedict) {
        checkPasswordsMatching();
        
        

        if(FacesContext.getCurrentInstance().isValidationFailed()) {
            return null;
        } else {
            
            UserAccount userAccount = userAccountDAO.findByPasswordRecoveryString(userManager.getSessionManager()
                    .getPasswordRecoveryString());
            
            if(userAccount != null) {
                userAccount.setUserPassword(newPassword);

                userAccountDAO.update(userAccount);

                userManager.getSessionManager().setBlocked(false);
                
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Password changed!", ""));
                return adressToRedict;
            }
            return null;
        }
    }

    private void checkPasswordsMatching() {
        if(newPassword.equals(newRepassword)) {
            return;
        }
        FacesContext.getCurrentInstance().addMessage("reseter:repasswd",
                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                "Passwords must agree!", ""));
        FacesContext.getCurrentInstance().validationFailed();
    }

    public String getNewRepassword() {
        return newRepassword;
    }

    public void setNewRepassword(String newRepassword) {
        this.newRepassword = newRepassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
