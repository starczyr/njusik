package reseter;

import java.util.UUID;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author radek
 */
public class PasswordReseter {

    public String getUniqueString() {
        String uuid = UUID.randomUUID().toString().replace("-", "");

        return uuid;
    }

    public void sendMailViaSSL(String recipient, String recoveryLink) {
        Properties properties = new Properties();

        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.socketFactory.port", "465");
        properties.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", "465");

        Session session = Session.getInstance(properties,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("newsikapp", "newsik1234");
            } 
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("newsikapp@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(recipient));
            message.setSubject("Newsik App Team");
            message.setText("Dear user,"
                    + "\n\n Here's ya link!\n\n" + recoveryLink);

            Transport.send(message);

            System.out.println("Done");

        } catch(MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}