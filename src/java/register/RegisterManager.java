package register;

import crypto.PasswordCrypto;
import exceptions.EntityAlreadyPersistedException;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import user.UserAccount;
import user.UserAccountDAO;

/**
 *
 *
 * @author Radek
 */
@Named(value = "registerManager")
@SessionScoped
public class RegisterManager implements Serializable {

    private UserAccountDAO userAccountDAO = new UserAccountDAO();
    private UIInput mailInput;
    private UIInput passwordInput;
    private UIInput repasswordInput;
    private UIInput nameInput;
    private UIInput lastNameInput;
    private String enteredMail;
    private String enteredPassword;
    private String enteredRepassword;
    private String enteredName;
    private String enteredLastName;
    private final boolean debug = true;

    /**
     * Creates a new instance of RegisterManager
     */
    public RegisterManager() {
    }

    /**
     * Save new account data to database.
     *
     * @return 'createHero' view
     */
    public String createNewAccount(String adressToRedirect) {

        checkPasswordsMatching();

        if(mailAval() == true && FacesContext.getCurrentInstance().isValidationFailed() == false) {
            UserAccount userAccount = new UserAccount(enteredMail, enteredPassword,
                    enteredName, enteredLastName);
            
            System.err.println("createAccount: " + enteredPassword);
            System.err.println("createAccount: " + new PasswordCrypto().passwordsEqual(new PasswordCrypto().encryptPassword(enteredPassword), enteredPassword));

            try {
                userAccountDAO.save(userAccount);
            } catch(EntityAlreadyPersistedException eape) {
                return null;
            } catch(Exception e) {
                return null;
            }


            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Success!", "Your account has been successfully created."));
            return adressToRedirect;
        }
        return null;
    }

    /**
     * Check if mail is already taken.
     *
     * @param actionEvent
     */
    public boolean mailAval() {

        AjaxRequestMailValidator armm = new AjaxRequestMailValidator();

        if(armm.isValidMail(enteredMail) == false) {
            return false;
        }

        UserAccount result = userAccountDAO.findByMail(enteredMail);

        if(result == null) {
            FacesContext.getCurrentInstance().addMessage("accountForm:mail",
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "This mail is available.", ""));
            return true;
        } else {
            FacesContext.getCurrentInstance().addMessage("accountForm:mail",
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                    result.getMail() + " is already taken!", ""));
            return false;
        }

    }

    /**
     * Sets every bean field to null.
     *
     * @return
     */
    public void clear() {
        this.enteredMail = null;
        this.enteredRepassword = null;
        this.enteredPassword = null;
        this.enteredName = null;
        this.enteredLastName = null;

        resetAll();
    }

    public UIInput getMailInput() {
        return mailInput;
    }

    public void setMailInput(UIInput mailInput) {
        this.mailInput = mailInput;
    }

    public UIInput getPasswordInput() {
        return passwordInput;
    }

    public void setPasswordInput(UIInput passwordInput) {
        this.passwordInput = passwordInput;
    }

    public UIInput getRepasswordInput() {
        return repasswordInput;
    }

    public void setRepasswordInput(UIInput repasswordInput) {
        this.repasswordInput = repasswordInput;
    }

    public UIInput getNameInput() {
        return nameInput;
    }

    public void setNameInput(UIInput nameInput) {
        this.nameInput = nameInput;
    }

    public UIInput getLastNameInput() {
        return lastNameInput;
    }

    public void setLastNameInput(UIInput lastNameInput) {
        this.lastNameInput = lastNameInput;
    }

    public void resetAll() {
        this.mailInput.resetValue();
        this.passwordInput.resetValue();
        this.repasswordInput.resetValue();
        this.nameInput.resetValue();
        this.lastNameInput.resetValue();
    }

    public String getEnteredMail() {
        return enteredMail;
    }

    public void setEnteredMail(String enteredMail) {
        this.enteredMail = enteredMail;
    }

    public String getEnteredPassword() {
        return enteredPassword;
    }

    public void setEnteredPassword(String enteredPassword) {
        this.enteredPassword = enteredPassword;
    }

    public String getEnteredRepassword() {
        return enteredRepassword;
    }

    public void setEnteredRepassword(String enteredRepassword) {
        this.enteredRepassword = enteredRepassword;
    }

    public String getEnteredName() {
        return enteredName;
    }

    public void setEnteredName(String enteredName) {
        this.enteredName = enteredName;
    }

    public String getEnteredLastName() {
        return enteredLastName;
    }

    public void setEnteredLastName(String enteredLastName) {
        this.enteredLastName = enteredLastName;
    }

    private void checkPasswordsMatching() {
        if(this.enteredPassword.equals(this.enteredRepassword)) {
            return;
        }
        FacesContext.getCurrentInstance().addMessage("accountForm:repasswd",
                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                "Passwords must agree!", ""));
        FacesContext.getCurrentInstance().validationFailed();
    }
}