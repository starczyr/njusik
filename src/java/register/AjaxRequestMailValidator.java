package register;

/**
 *
 * @author radek
 */
public class AjaxRequestMailValidator {

    public AjaxRequestMailValidator() {
    }
    public boolean isValidMail(String mail) {
        if(mail.matches("[a-zA-Z0-9._%+-]+@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,4}") && !(mail.length() > 50)) {
            return true;
        } else {
            return false;
        }
    }
}
