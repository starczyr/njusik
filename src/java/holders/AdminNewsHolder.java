package holders;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import news.News;
import news.NewsDAO;

/**
 *
 * @author radek
 */
@Named(value = "adminNewsHolder")
@SessionScoped
public class AdminNewsHolder implements Serializable {

    private final int MAX_NUMBER_OF_NEWS_IN_VIEW = 5;
    private NewsDAO newsDAO = new NewsDAO();
    private int dbSize = readDBSize();
    private int numberOfAcceptedNews = readAcceptedNumber();
    private int numberOfUnacceptedNews = readUnacceptedNumber();
    private int portionFrom = 0;
    private int portionTo = MAX_NUMBER_OF_NEWS_IN_VIEW < dbSize ? MAX_NUMBER_OF_NEWS_IN_VIEW : dbSize;

    public AdminNewsHolder() {
    }

    public void loadNextPortion() {
        if(portionTo + MAX_NUMBER_OF_NEWS_IN_VIEW < dbSize) {
            portionFrom += MAX_NUMBER_OF_NEWS_IN_VIEW;
            portionTo += MAX_NUMBER_OF_NEWS_IN_VIEW;
        } else if(portionTo == dbSize) {
        } else {
            portionFrom = portionTo;
            portionTo = dbSize;
        }

    }

    public void loadPreviousPortion() {
        if(portionTo == dbSize) {
            portionTo = portionFrom;
            portionFrom -= MAX_NUMBER_OF_NEWS_IN_VIEW;
        } else {
            if(portionFrom - MAX_NUMBER_OF_NEWS_IN_VIEW > 0) {
                portionFrom -= MAX_NUMBER_OF_NEWS_IN_VIEW;
                portionTo -= MAX_NUMBER_OF_NEWS_IN_VIEW;
            } else {
                portionFrom = 0;
                portionTo = MAX_NUMBER_OF_NEWS_IN_VIEW < dbSize ? MAX_NUMBER_OF_NEWS_IN_VIEW : dbSize;
            }
        }
    }

    public List<News> getLastPortionOfUnacceptedNews() {
        List<News> lastPortion = newsDAO.getPortionOfUnaccepted(portionFrom, portionTo);
        return lastPortion;
    }

    public List<News> getLastPortionOfAcceptedNews() {
        List<News> lastPortion = newsDAO.getPortionOfAccepted(portionFrom, portionTo);
        return lastPortion;
    }

    public List<News> getLastPortionOfNews() {
        List<News> lastPortion = newsDAO.getPortion(portionFrom, portionTo);
        return lastPortion;
    }

    public int getNumberOfNewsCurrentlyInView() {
        return portionTo - portionFrom;
    }

    public int getPortionFrom() {
        return portionFrom;
    }

    public int getPortionTo() {
        return portionTo;
    }

    public int getNumberOfAcceptedNews() {
        return numberOfAcceptedNews;
    }

    public void setNumberOfAcceptedNews(int numberOfAcceptedNews) {
        this.numberOfAcceptedNews = numberOfAcceptedNews;
    }

    public int getNumberOfUnacceptedNews() {
        return numberOfUnacceptedNews;
    }

    public void setNumberOfUnacceptedNews(int numberOfUnacceptedNews) {
        this.numberOfUnacceptedNews = numberOfUnacceptedNews;
    }

    public int getDbSize() {
        return dbSize;
    }

    public void setDbSize(int dbSize) {
        this.dbSize = dbSize;
    }

    public int readDBSize() {
        int size = newsDAO.getDBSize();
        dbSize = size;
        return size;
    }

    public int readAcceptedNumber() {
        int accepted = newsDAO.getAcceptedNumber();
        numberOfAcceptedNews = accepted;
        return accepted;
    }

    public int readUnacceptedNumber() {
        int unaccepted = newsDAO.getUnacceptedNumber();
        numberOfUnacceptedNews = unaccepted;
        return unaccepted;
    }

    public void updateSizes() {
        dbSize = readDBSize();
        numberOfAcceptedNews = readAcceptedNumber();
        numberOfUnacceptedNews = readUnacceptedNumber();
    }
}
