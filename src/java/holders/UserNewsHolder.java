package holders;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import news.News;
import news.NewsDAO;

/**
 *
 * @author radek
 */
@Named(value = "userNewsHolder")
@SessionScoped
public class UserNewsHolder implements Serializable {

    private NewsDAO newsDAO = new NewsDAO();
    private int portionFrom = 0;
    private int portionTo = 5;
    private int numberOfNewsToShow = readNumberOfNewsToShow();

    public UserNewsHolder() {
    }

    public void loadNextPortion() {

        if(portionTo + 6 < numberOfNewsToShow) {
            portionFrom += 6;
            portionTo += 6;
        } else if(portionTo == numberOfNewsToShow) {
        } else {
            portionFrom = portionTo;
            portionTo = numberOfNewsToShow;
        }

    }

    public void loadPreviousPortion() {
        if(portionFrom - 6 >= 0) {
            portionFrom -= 6;
            portionTo -= 6;
        } else {
            portionFrom = 0;
            portionTo = 5;
        }
    }

    public int getNumberOfNewsToShow() {
        return numberOfNewsToShow;
    }

    public void setNumberOfNewsToShow(int numberOfNewsToShow) {
        this.numberOfNewsToShow = numberOfNewsToShow;
    }

    public int readNumberOfNewsToShow() {
        return newsDAO.getAcceptedNumber();
    }

    public List<News> getLastPortionOfAcceptedNews() {
        List<News> lastPortion = newsDAO.getPortionOfAccepted(portionFrom, portionTo);
        return lastPortion;
    }
}
