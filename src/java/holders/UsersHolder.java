package holders;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Named;
import user.UserAccount;
import user.UserAccountDAO;

/**
 *
 * @author radek
 */
@Named(value = "usersHolder")
@SessionScoped
public class UsersHolder implements Serializable {

    private final int MAX_NUMBER_OF_USERS_IN_VIEW = 5;
    private UserAccountDAO userAccountDAO = new UserAccountDAO();
    private int dbSize = readDBSize();
    private int numberOfAdmins = readAdminNumber();
    private int numberOfPlainUsers = readPlainUsersNumber();
    private int portionFrom = 0;
    private int portionTo = (MAX_NUMBER_OF_USERS_IN_VIEW < dbSize) ? MAX_NUMBER_OF_USERS_IN_VIEW : dbSize;

    public UsersHolder() {
    }
   

    public void makeAdmin(Integer idUserAccount) {
        UserAccount userAccount = userAccountDAO.findById(idUserAccount);
        userAccount.setIsAdmin(true);
        userAccountDAO.update(userAccount);
    }

    public void takeAdminRights(Integer idUserAccount) {
        UserAccount userAccount = userAccountDAO.findById(idUserAccount);
        userAccount.setIsAdmin(false);
        userAccountDAO.update(userAccount);
    }

    public void loadPreviousPortion() {
        if(portionTo == dbSize) {
            portionTo = portionFrom;
            portionFrom -= MAX_NUMBER_OF_USERS_IN_VIEW;
        } else {
            if(portionFrom - MAX_NUMBER_OF_USERS_IN_VIEW > 0) {
                portionFrom -= MAX_NUMBER_OF_USERS_IN_VIEW;
                portionTo -= MAX_NUMBER_OF_USERS_IN_VIEW;
            } else {
                portionFrom = 0;
                portionTo = (MAX_NUMBER_OF_USERS_IN_VIEW < dbSize) ? MAX_NUMBER_OF_USERS_IN_VIEW : dbSize;
            }
        }
    }

    public void loadNextPortion() {
        if(portionTo + MAX_NUMBER_OF_USERS_IN_VIEW < dbSize) {
            portionFrom += MAX_NUMBER_OF_USERS_IN_VIEW;
            portionTo += MAX_NUMBER_OF_USERS_IN_VIEW;
        } else if(portionTo == dbSize) {
        } else {
            portionFrom = portionTo;
            portionTo = dbSize;
        }
    }

    public List<UserAccount> getLastPortionOfUsers() {
        List<UserAccount> portion = userAccountDAO.getPortion(portionFrom, portionTo);
        return portion;
    }

    public void updateSizes() {
        dbSize = readDBSize();
        numberOfAdmins = readAdminNumber();
        numberOfPlainUsers = readPlainUsersNumber();
    }

    private int readPlainUsersNumber() {
        return userAccountDAO.getPlainUsersNumber();
    }

    private int readAdminNumber() {
        return userAccountDAO.getAdminsNumber();
    }

    private int readDBSize() {
        return userAccountDAO.getDBSize();
    }

    public int getPortionFrom() {
        return portionFrom;
    }

    public int getPortionTo() {
        return portionTo;
    }

    public int getDbSize() {
        return dbSize;
    }

    public void setDbSize(int dbSize) {
        this.dbSize = dbSize;
    }

    public int getNumberOfAdmins() {
        return numberOfAdmins;
    }

    public void setNumberOfAdmins(int numberOfAdmins) {
        this.numberOfAdmins = numberOfAdmins;
    }

    public int getNumberOfPlainUsers() {
        return numberOfPlainUsers;
    }

    public void setNumberOfPlainUsers(int numberOfPlainUsers) {
        this.numberOfPlainUsers = numberOfPlainUsers;
    }
}
