package filler;

import java.util.Date;
import java.util.Random;

/**
 * Test-helping class, which randomly chooses title from a set of words.
 *
 * @author radek
 */
public class TitleFiller {
    private static TitleFiller titleFiller = new TitleFiller();
    private String[] fragments = {"another", "simple", "test", "plain"};
    
    private TitleFiller() {
        
    }
    public static TitleFiller getTitleFiller() {
        return titleFiller;
    }
    public String generateTitle() {
        Random random = new Random(new Date().getTime());
        StringBuilder title = new StringBuilder();
        title.append(fragments[random.nextInt(fragments.length)]);
        title.append(" title");
        
        return title.toString();
    }
}
