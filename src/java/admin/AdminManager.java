package admin;

import holders.AdminNewsHolder;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import news.News;
import news.NewsDAO;
import holders.UsersHolder;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author radek
 */
@Named(value = "adminManager")
@SessionScoped
public class AdminManager implements Serializable {

    //private AdminNewsHolder adminNewsHolder = new AdminNewsHolder();
    //private UsersHolder usersHolder = new UsersHolder();
    @Inject
    AdminNewsHolder adminNewsHolder;
    @Inject
    UsersHolder usersHolder;
    private boolean loggedAsAdmin;
    private Map<Integer, Boolean> selectedNews = new HashMap<>();
    private int numberOfSelectedNews;
    private NewsDAO newsDAO = new NewsDAO();

    public AdminManager() {
    }

    public Map<Integer, Boolean> getSelectedNews() {
        return selectedNews;
    }

    public void setSelectedNews(Map<Integer, Boolean> selectedNews) {
        this.selectedNews = selectedNews;
    }

    public void deleteSelectedNews() {
        Set<Integer> deletedIds = new HashSet<>();
        for(Integer integer : selectedNews.keySet()) {
            if(selectedNews.get(integer)) {
                deletedIds.add(integer);
            }
        }
        delete(deletedIds);
        adminNewsHolder.updateSizes();
    }

    public void discardSelectedNews() {
        Set<Integer> discardedIds = new HashSet<>();
        for(Integer integer : selectedNews.keySet()) {
            if(selectedNews.get(integer)) {
                discardedIds.add(integer);
            }
        }
        markAsDiscarded(discardedIds);
        adminNewsHolder.updateSizes();
    }

    public void acceptSelectedNews() {
        Set<Integer> acceptedIds = new HashSet<>();
        for(Integer integer : selectedNews.keySet()) {
            if(selectedNews.get(integer)) {
                acceptedIds.add(integer);
            }
        }
        markAsAccepted(acceptedIds);
        adminNewsHolder.updateSizes();
    }

    private void delete(Set<Integer> deleted) {
        for(Integer id : deleted) {
            News n = newsDAO.findById(id);
            newsDAO.delete(n);
        }
    }

    private void markAsDiscarded(Set<Integer> discardedIds) {
        for(Integer id : discardedIds) {
            News n = newsDAO.findById(id);
            n.setAcceptedByAdmin(false);
            newsDAO.update(n);
        }
    }

    private void markAsAccepted(Set<Integer> acceptedNews) {
        for(Integer id : acceptedNews) {
            News n = newsDAO.findById(id);
            n.setAcceptedByAdmin(true);
            newsDAO.update(n);
        }
    }

    public UsersHolder getUsersHolder() {
        return usersHolder;
    }

    public AdminNewsHolder getAdminNewsHolder() {
        return adminNewsHolder;
    }

    public int getNumberOfSelectedNews() {
        return numberOfSelectedNews;
    }

    public void setNumberOfSelectedNews(int numberOfSelectedNews) {
        this.numberOfSelectedNews = numberOfSelectedNews;
    }

    public boolean isLoggedAsAdmin() {
        return loggedAsAdmin;
    }

    public void setLoggedAsAdmin(boolean loggedAsAdmin) {
        this.loggedAsAdmin = loggedAsAdmin;
    }

    public void logAsAdmin() throws IOException {
        loggedAsAdmin = true;
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext context = (ServletContext) facesContext.getExternalContext().getContext();
        facesContext.getExternalContext().redirect(context.getContextPath()
                .concat("/Admin/adminPanel.xhtml"));
    }

    public void logOutFromAdmin() throws IOException {
        loggedAsAdmin = false;
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext context = (ServletContext) facesContext.getExternalContext().getContext();
        facesContext.getExternalContext().redirect(context.getContextPath()
                .concat("/index.xhtml"));
    }
}
