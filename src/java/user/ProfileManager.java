package user;

import java.util.Set;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author radek
 */
@Named(value = "profileManager")
@Dependent
public class ProfileManager {

//    @Inject
//    UserAccount userAccount;
    private StatusDAO statusDAO = new StatusDAO();
    private Set<Status> socials;

    public ProfileManager() {
    }

    public Set<Status> getSocials() {
     
        return socials;
    }
}