package user;

import java.io.Serializable;
import java.util.Date;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import session.SessionManager;

/**
 *
 * @author radek
 */
@Named(value = "userManager")
@SessionScoped
public class UserManager implements Serializable {

    private SessionManager sessionManager = SessionManager.getSessionManager();
    private Date whenBlocked;
    private String newStatus;
    private int numberOfLoginTries;

    public UserManager() {
    }

    public HttpServletRequest getRequest() {
        return ((HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest());
    }

    public Date getWhenBlocked() {
        return whenBlocked;
    }

    public void setWhenBlocked(Date whenBlocked) {
        this.whenBlocked = whenBlocked;
    }

    public SessionManager getSessionManager() {
        return sessionManager;
    }

    public String getUserMailFromId(int id) {
        UserAccountDAO userAccountDAO = new UserAccountDAO();

        UserAccount userAccount = userAccountDAO.findById(id);

        return userAccount.getMail();
    }

    public void changeStatus() {
        if (newStatus.equals("") || FacesContext.getCurrentInstance().isValidationFailed()) {
            return;
        }

        newStatus = Jsoup.clean(newStatus, Whitelist.basic());

        sessionManager.setStatus(newStatus);
    }

    public int getNumberOfLoginTries() {
        return numberOfLoginTries;
    }

    public void setNumberOfLoginTries(int numberOfLoginTries) {
        this.numberOfLoginTries = numberOfLoginTries;
    }

    public String getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(String newStatus) {
        this.newStatus = newStatus;
    }

    @Override
    public String toString() {
        return "UserManager{" + "newStatus=" + newStatus + ", numberOfLoginTries=" + numberOfLoginTries + '}';
    }
}