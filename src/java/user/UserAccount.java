package user;

import crypto.PasswordCrypto;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import org.hibernate.annotations.Entity;
import org.hibernate.annotations.Table;

/**
 *
 * @author radek
 */
@Entity
@Table(appliesTo = "userAccount")
public class UserAccount implements Serializable {

    
    private Integer idUserAccount;
    private String mail;
    private String userPassword;
    private String name;
    private String lastName;
    private String passwordRecoveryString;
    private boolean isAdmin;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "userAccount", cascade = CascadeType.ALL)
    @JoinColumn(name = "id_user_account")
    private Set<Status> statuses = new HashSet<>();

    

    public UserAccount() {
    }

    public UserAccount(String mail, String userPassword, String name, String lastName) {
        this.mail = mail;
        PasswordCrypto crypto = new PasswordCrypto();
        String passwordToStore = crypto.encryptPassword(userPassword);
        this.userPassword = passwordToStore;
        this.name = name;
        this.lastName = lastName;
    }

    public Set<Status> getStatuses() {
        return statuses;
    }

    public void setStatuses(Set<Status> statuses) {
        this.statuses = statuses;
    }
    
    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        PasswordCrypto crypto = new PasswordCrypto();
        String passwordToStore = crypto.encryptPassword(userPassword);
        this.userPassword = passwordToStore;
    }

    public Integer getIdUserAccount() {
        return idUserAccount;
    }

    public void setIdUserAccount(Integer idUserAccount) {
        this.idUserAccount = idUserAccount;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPasswordRecoveryString() {
        return passwordRecoveryString;
    }

    public void setPasswordRecoveryString(String passwordRecoveryString) {
        this.passwordRecoveryString = passwordRecoveryString;
    }

    public boolean isIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + (this.idUserAccount != null ? this.idUserAccount.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) {
            return false;
        }
        if(getClass() != obj.getClass()) {
            return false;
        }
        final UserAccount other = (UserAccount) obj;
        if(this.idUserAccount != other.idUserAccount && (this.idUserAccount == null || !this.idUserAccount.equals(other.idUserAccount))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UserAccount{" + "idUserAccount=" + idUserAccount + ", mail=" + mail + ", userPassword=" + userPassword + ", name=" + name + ", lastName=" + lastName + ", passwordRecoveryString=" + passwordRecoveryString + ", isAdmin=" + isAdmin + '}';
    }
    
    
}