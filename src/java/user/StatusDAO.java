package user;

import exceptions.EntityAlreadyPersistedException;
import hibernate.DAO;
import hibernate.HibernateUtils;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author radek
 */
public class StatusDAO extends DAO<Status> {

    @Override
    public Status findById(int t) {
        return null;
    }
    public List<Status> findAllByUserId(int userId) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();

        List<Status> result = session
                .createQuery("from Status s where s.id_user_account = :userId")
                .setParameter("userId", userId).list();

        session.getTransaction().commit();

        return result;
    }
    
    public Status findLastByUserId(int userId) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();

        Status result = (Status) session
                .createQuery("select max(s.id_status) from Status s where s.id_user_account = :userId ")
                .setParameter("userId", userId).uniqueResult();

        session.getTransaction().commit();

        return result;
    }
public List<Status> findPortionByUserId(int userId, int from, int to) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();

        List<Status> result = session
                .createQuery("from Status s where s.id_user_account = :userId").setFirstResult(from).setMaxResults(to)
                .setParameter("userId", userId).list();

        session.getTransaction().commit();

        return result;
    }

    
    public static void main(String[] args) throws EntityAlreadyPersistedException {
        StatusDAO statusDAO = new StatusDAO();
        UserAccountDAO userAccountDAO = new UserAccountDAO();
        
        UserAccount userAccount = userAccountDAO.findByMail("radek.starczynowski@gmail.com");
        
        Status status = new Status();
        status.setAddedOn(new Date());
        status.setText("Some status");
        
        statusDAO.save(status);
        
      
        
    }
    
}
