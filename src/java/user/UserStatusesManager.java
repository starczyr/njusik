package user;

import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.Dependent;

/**
 *
 * @author radek
 */
@Named(value = "userStatusesManager")
@Dependent
public class UserStatusesManager {

//    @Inject
//    private UserAccount userAccount;
    private final int STATUSES_PORTION_SIZE = 5;
    private StatusDAO statusDAO = new StatusDAO();
    private Status lastStatus;
    private List<Status> lastStatusesPortion;

    /**
     * Creates a new instance of UserStatusesManager
     */
    public UserStatusesManager() {
    }

//    public Status getLastStatus() {
////        if(lastStatus == null) {
////            return statusDAO.findLastByUserId(userAccount.getIdUserAccount());
////        } else {
//            return lastStatus;
////        }
//    }
//
//    public void setLastStatus(Status lastStatus) {
//        this.lastStatus = lastStatus;
//    }
//
//    public List<Status> getLastStatusesPortion() {
//        if(lastStatusesPortion == null) {
//            return statusDAO.findPortionByUserId(userAccount.getIdUserAccount(), 0, STATUSES_PORTION_SIZE);
//        } else {
//            return lastStatusesPortion;
//        }
//    }
//
//    public void setLastStatusesPortion(List<Status> lastStatusesPortion) {
//        this.lastStatusesPortion = lastStatusesPortion;
//    }
}
