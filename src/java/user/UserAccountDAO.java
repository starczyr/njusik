package user;

import exceptions.EntityAlreadyPersistedException;
import hibernate.DAO;
import hibernate.HibernateUtils;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;

/**
 * Data Access Object design pattern implementation.
 *
 * @author radek
 */
public class UserAccountDAO extends DAO<UserAccount> {

    @Override
    public UserAccount findById(int id) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();

        UserAccount result = (UserAccount) session.get(UserAccount.class, id);

        session.getTransaction().commit();

        return result;
    }

    public int getDBSize() {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();
        int size = ((Long) session.createQuery("select count(*) from UserAccount").iterate().next()).intValue();

        return size;
    }

    public int getAdminsNumber() {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();

        int size = ((Long) session.createQuery("select count(*) from UserAccount u where u.isAdmin = 1")
                .iterate().next()).intValue();

        return size;
    }

    public int getPlainUsersNumber() {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();

        int size = ((Long) session.createQuery("select count(*) from UserAccount u where u.isAdmin = 0")
                .iterate().next()).intValue();

        return size;
    }

    public List<UserAccount> getPortion(int from, int to) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();

        List<UserAccount> users = session.createQuery("from UserAccount u").setFirstResult(from).setMaxResults(to).list();

        session.getTransaction().commit();

        return users;

    }

    public UserAccount findByMail(String mail) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();

        UserAccount result = (UserAccount) session
                .createQuery("from UserAccount as u where u.mail = :mail")
                .setParameter("mail", mail).uniqueResult();

        session.getTransaction().commit();

        return result;
    }

    public UserAccount findByPasswordRecoveryString(String string) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();

        UserAccount result = (UserAccount) session
                .createQuery("select u from UserAccount u where u.passwordRecoveryString = :passwordRecoveryString")
                .setParameter("passwordRecoveryString", string).uniqueResult();

        session.getTransaction().commit();

        return result;
    }

    private String generateRandomString(int length) {
        Random random = new Random(new Date().getTime());

        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < length; i++) {
            int number = 97 + random.nextInt(25);
            stringBuilder.append((char) number);
        }
        return stringBuilder.toString();
    }

    public static void main(String[] args) {
        try {
            UserAccountDAO userAccountDAO = new UserAccountDAO();
            UserAccount userAccount = new UserAccount("radek.starczynowski@gmail.com", "radek", "Radek", "Starczynowski");
            userAccount.setIsAdmin(true);
            userAccountDAO.save(userAccount);
            
            Set statuses = new HashSet();
            Status status = new Status();
            status.setAddedOn(Calendar.getInstance().getTime());
            status.setText("First status!");
            status.setUserAccount(userAccount);
            
            
            statuses.add(status);
            userAccount.setStatuses(statuses);
            
            StatusDAO statusDAO = new StatusDAO();
            statusDAO.save(status);
            

            UserAccount ua = userAccountDAO.findById(1);
            
            Set set = ua.getStatuses();

            for(Object object : set) {
                System.err.println(object.toString());
            }
            
            


//            UserAccount userAccount = new UserAccount(userAccountDAO.generateRandomString(5),
//                    userAccountDAO.generateRandomString(5),
//                    userAccountDAO.generateRandomString(5),
//                    userAccountDAO.generateRandomString(5));
//            

            
                    
                    //userAccountDAO.findByMail("radek.starczynowski@gmail.com");
//Set st = userAccount.getStatuses();
//            for(Object object : st) {
//                System.err.println(object.toString());
//            }


            
            
            


            
            
        } catch(EntityAlreadyPersistedException ex) {
            Logger.getLogger(UserAccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }


    }
}