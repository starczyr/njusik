package user;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Entity;

/**
 *
 * @author radek
 */
@Entity
public class Status {

    private Integer idStatus;
    private Date addedOn;
    private String text;
    @ManyToOne
    @JoinColumn(name = "user_account_fk", nullable = false)
    private UserAccount userAccount;
    
    public Status() {
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }
    
    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Status{" + "addedOn=" + addedOn + ", text=" + text + '}';
    }
    
    
    public static void main(String[] args) {
        Status status = new Status();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        System.err.println(dateFormat.format(calendar.getTime()));
    }
}
