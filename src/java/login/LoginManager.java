package login;

import admin.AdminManager;
import crypto.PasswordCrypto;
import error.ErrorManager;
import error.ErrorUtils;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import reseter.PasswordReseter;
import user.UserAccount;
import user.UserAccountDAO;
import user.UserManager;

/**
 *
 * @author radek
 */
@Named(value = "loginManager")
@SessionScoped
public class LoginManager implements Serializable {

    @Inject
    UserManager userManager;
    @Inject
    ErrorManager errorManager;
    EntityManager entityManager;
    UserAccountDAO userAccountDAO = new UserAccountDAO();
    private String enteredMail;
    private String enteredPassword;
    private AdminManager adminManager;
    private boolean forgot;
    private boolean inDatabase;

    public LoginManager() {
    }

    public void logOut() throws IOException {
        ((HttpSession) FacesContext.getCurrentInstance().getExternalContext()
                .getSession(false)).invalidate();
        userManager.getSessionManager().endSession();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext context = (ServletContext) facesContext.getExternalContext().getContext();
        facesContext.getExternalContext().redirect(context.getContextPath()
                .concat("/index.xhtml"));
    }

    public void tooManyTriesResponse() {
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance()
                .getExternalContext().getResponse();

        ErrorUtils.getErrorUtils().setErrorMessages(ErrorUtils.Reason.TOO_MANY_TRIES);

        try {
            response.sendRedirect("/Newsik/error.xhtml");
        } catch(IOException ex) {
            Logger.getLogger(LoginManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String logIn(String pageToRedirect) {
        FacesContext context = FacesContext.getCurrentInstance();



        if(context.isValidationFailed()) {
            return null;
        }

        if(userManager.getNumberOfLoginTries() > 2) {
            if(userManager.getSessionManager().isBlocked()) {
                if(errorManager.getMinutesToWait() > 0) {
                    tooManyTriesResponse();
                }
            } else {
                userManager.getSessionManager().setBlocked(true);
                userManager.setWhenBlocked(new Date());
            }
            return null;
        }

        UserAccount result = userAccountDAO.findByMail(enteredMail);

        if(result == null) {
            inDatabase = false;
            context.addMessage("logInForm:enteredMail",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "There is no " + this.enteredMail + " in the database!", ""));
            context.validationFailed();

        } else {

            inDatabase = true;

            PasswordCrypto crypto = new PasswordCrypto();

            if(crypto.passwordsEqual(result.getUserPassword(), enteredPassword) == false
                    && enteredPassword.equals("") == false) {
                context.addMessage("logInForm:enteredPassword",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Wrong password for " + this.enteredMail + "!", ""));
                context.validationFailed();

                int numberOfLoginTries = userManager.getNumberOfLoginTries();
                numberOfLoginTries++;
                userManager.setNumberOfLoginTries(numberOfLoginTries);

            } else {
                if(context.isValidationFailed() == false) {
                    startSession(result);
                    context.addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Welcome " + userManager.getSessionManager().getName(),
                            "We proudly greet You in Newsik!"));
                }
            }
        }
        if(context.isValidationFailed()) {
            return null;
        }
        return pageToRedirect;
    }

    public void startSession(UserAccount user) {
        userManager.getSessionManager().setBlocked(false);

        userManager.getSessionManager().setLoggedIn(true);
        userManager.getSessionManager().setUserMail(user.getMail());
        userManager.getSessionManager().setName(user.getName());
        userManager.getSessionManager().setLastName(user.getLastName());
        userManager.getSessionManager().setIdUser(user.getIdUserAccount());

        // ##
        userManager.getSessionManager()
                .setStatus("Sample status but now there is a very long one, "
                + "and longer, ang longer, and multi-lined!");
        // ##

        if(user.isIsAdmin()) {
            userManager.getSessionManager().setAdmin(true);
        }
    }

    public String sendReseter(String adressToRedirect) {
        forgot = false;
        PasswordReseter passwordRecoverer = new PasswordReseter();
        String uniqueString = passwordRecoverer.getUniqueString();


        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance()
                .getExternalContext().getContext();

        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance()
                .getExternalContext().getRequest();

        StringBuffer urlBuffered = request.getRequestURL();

        String url = urlBuffered.append("?string=").append(uniqueString)
                .toString().replace("logIn.xhtml", "reseter.xhtml");

        UserAccount userAccount = userAccountDAO.findByMail(enteredMail);

        if(userAccount == null) {
            FacesContext.getCurrentInstance().addMessage("logInForm:enteredPassword", new FacesMessage(FacesMessage.SEVERITY_FATAL,
                    "Aw!", "It wasn't funny."));
            return null;
        }



        try {
            userAccount.setPasswordRecoveryString(uniqueString);
            userAccountDAO.update(userAccount);
        } catch(Exception e) {
            FacesContext.getCurrentInstance().addMessage("logInForm:enteredPassword", new FacesMessage(FacesMessage.SEVERITY_FATAL,
                    "Ups!", "Something went terribly wrong. We're working on that."));
            return null;

        }


        passwordRecoverer.sendMailViaSSL(userAccount.getMail(), url);

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                "Hey!", "Check Your mail."));
        return adressToRedirect;



    }

    public void dontSend() {
        System.err.println("dont send it");
        forgot = false;
    }

    public boolean isInDatabase() {
        return inDatabase;
    }

    public void setInDatabase(boolean inDatabase) {
        this.inDatabase = inDatabase;
    }

    public UserManager getUserManager() {
        return userManager;
    }

    public void yesIForgot() {
        forgot = true;
    }

    public boolean isForgot() {
        return forgot;
    }

    public void setForgot(boolean forgot) {
        this.forgot = forgot;
    }

    public AdminManager getAdminManager() {
        return adminManager;
    }

    public String getEnteredMail() {
        return enteredMail;
    }

    public void setEnteredMail(String enteredMail) {
        this.enteredMail = enteredMail;
    }

    public String getEnteredPassword() {
        return enteredPassword;
    }

    public void setEnteredPassword(String enteredPassword) {
        this.enteredPassword = enteredPassword;
    }

    @Override
    public String toString() {
        return "LoginManager{" + ", enteredMail=" + enteredMail
                + ", enteredPassword=" + enteredPassword + ", forgot=" + forgot + '}';
    }
}
