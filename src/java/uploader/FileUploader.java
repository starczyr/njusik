package uploader;

import java.io.IOException;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author radek
 */
@Named(value = "fileUploader")
@SessionScoped
public class FileUploader implements Serializable {

    private UploadedFile uploadedFile;
    // TODO: remove this later        
    private boolean debug = true;

    public FileUploader() {
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public void uploadFileHandler(FileUploadEvent fileUploadEvent) {
        setUploadedFile(fileUploadEvent.getFile());
        if(uploadedFile != null) {
            FacesMessage msg = new FacesMessage("",
                    "Your file has been successfully uploaded");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

  

    public void uploadFile(UploadedFile uploadedFile) throws IOException {


        String path = ((ServletContext) FacesContext.getCurrentInstance()
                .getExternalContext().getContext()).getRealPath("/users");

        if(debug) {
            System.err.println("GetRealPath: " + path);
            System.err.println("after getting .../img path");
        }

        //String type = uploadedFile.getContentType().replaceFirst(".*/", "");


//        String newsID = String.valueOf(createdNews.getNewsId());
//
//        String name = "(" + newsID + ")" + createdNews.getTitle();
//        name = name.replace(' ', '_');
//
//
//        String fileName = name.concat("." + type);
//
//
//        File folderImg = new File(path);
//        
//        String[] content = folderImg.list();
//        List<String> contentList = Arrays.asList(content);
//
//        File newsUniqueFolder = new File(folderImg, name);
//
//        if(contentList.contains(name) == false) {
//            newsUniqueFolder.mkdir();
//        }
//        if(debug) {
//            System.err.println("trying to make some unique name: "
//                    + "\"" + fileName + "\"");
//        }
//        
//        path = newsUniqueFolder.getAbsolutePath();
//        if(debug) {
//            System.err.println("should be equals to: " + newsUniqueFolder);
//        }
//        File pathWithFilename = new File(path, fileName);
//
//        if(debug) {
//            System.err.println("getting full path as: " + pathWithFilename);
//            System.err.println("getting type as: " + type);
//        }
//
//        InputStream inputStream = uploadedFile.getInputstream();
//        BufferedImage bufferedImage = ImageIO.read(inputStream);
//
//        System.err.println("inputstream: " + inputStream.toString());
//        System.err.println("bufferedimage: " + bufferedImage.toString());
//
//        ImageIO.write(bufferedImage, type, pathWithFilename);
//
//        if(debug) {
//            BufferedImage os = ImageIO.read(pathWithFilename);
//        }
    }
}
