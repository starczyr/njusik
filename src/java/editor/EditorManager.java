package editor;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import user.UserManager;

/**
 *
 * @author radek
 */
@Named(value = "editorManager")
@SessionScoped
public class EditorManager implements Serializable {

    @Inject
    UserManager userManager;

    public EditorManager() {
    }  
}