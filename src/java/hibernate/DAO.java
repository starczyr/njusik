package hibernate;

import exceptions.EntityAlreadyPersistedException;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;

/**
 * Data Access Object, a base class for all DAO classes in this project.
 *
 * @author radek
 */
public abstract class DAO<T> {

    public abstract T findById(int t);

    public void save(T t) throws EntityAlreadyPersistedException {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        try {
            session.beginTransaction();

            session.save(t);

            session.getTransaction().commit();
        } catch(ConstraintViolationException e) {
            if(session.getTransaction() != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if(session.isOpen()) {
                session.close();
            }
        }
    }

    public void update(T t) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();

        session.update(t);

        session.getTransaction().commit();
    }

    public void delete(T t) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();

        session.delete(t);

        session.getTransaction().commit();
    }
}
