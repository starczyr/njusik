package news;

import exceptions.EntityAlreadyPersistedException;
import filler.TitleFiller;
import hibernate.DAO;
import hibernate.HibernateUtils;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author radek
 */
public class NewsDAO extends DAO<News> {

    public int getDBSize() {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();
        int size = ((Long) session.createQuery("select count(*) from News").iterate().next()).intValue();

        return size;
    }

    public int getAcceptedNumber() {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();

        int size = ((Long) session.createQuery("select count(*) from News n where n.acceptedByAdmin = 1")
                .iterate().next()).intValue();

        return size;
    }

    public int getUnacceptedNumber() {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();

        int size = ((Long) session.createQuery("select count(*) from News n where n.acceptedByAdmin = 0")
                .iterate().next()).intValue();

        return size;
    }

    @Override
    public News findById(int id) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();

        News result = (News) session.get(News.class, id);

        session.getTransaction().commit();

        return result;
    }

    public List<News> getPortion(int from, int to) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();

        List<News> news = session.createQuery("select n from News n order by n.idNews desc")
                .setFirstResult(from).setMaxResults(to).list();

        session.getTransaction().commit();

        return news;
    }

    public List<News> getPortionOfAccepted(int from, int to) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();

        List<News> news = session.createQuery("select n from News n where n.acceptedByAdmin = 1")
                .setFirstResult(from).setMaxResults(to).list();

        session.getTransaction().commit();

        return news;
    }

    public List<News> getPortionOfUnaccepted(int from, int to) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();

        session.beginTransaction();

        List<News> news = session.createQuery("select n from News n where n.acceptedByAdmin = 0")
                .setFirstResult(from).setMaxResults(to).list();

        session.getTransaction().commit();

        return news;
    }

    public static void main(String[] args) throws EntityAlreadyPersistedException {
        NewsDAO newsDAO = new NewsDAO();
        TitleFiller titleFiller = TitleFiller.getTitleFiller();
        for(int i = 0; i < 12; i++) {
            News news = new News();
            news.setCreatedOn(new Date());
            news.setReporterId(1);
            news.setTitle(titleFiller.generateTitle());
            news.setText("Lorem ipsum dolor sit amet, consectetur adipisicing elit. "
                    + "Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. "
                    + "Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam. Quisque "
                    + "semper justo at risus. Donec venenatis, turpis vel hendrerit interdum, dui "
                    + "ligula ultricies purus, sed posuere libero dui id orci. Nam congue, pede vitae "
                    + "dapibus aliquet, elit magna vulputate arcu, vel tempus metus leo non est. "
                    + "Etiam sit amet lectus quis est congue mollis. Phasellus congue lacus eget neque. "
                    + "Phasellus ornare, ante vitae consectetuer consequat, purus sapien ultricies dolor, "
                    + "et mollis pede metus eget nisi. Praesent sodales velit quis augue. Cras suscipit, "
                    + "urna at aliquam rhoncus, urna quam viverra nisi, in interdum massa nibh nec erat.");
            newsDAO.save(news);
        }
    }
}
