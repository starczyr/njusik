package news;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author radek
 */
public class News implements Serializable {

    private Integer idNews;
    private String title;
    private String text;
    private Date createdOn;
    private String tags;
    private Integer reporterId;
    private String adminId;
    private boolean acceptedByAdmin;

    public News() {
    }

    public News(String title, String text, Date createdOn, String tags, Integer reporterId, String adminId) {
        this.title = title;
        this.text = text;
        this.createdOn = createdOn;
        this.tags = tags;
        this.reporterId = reporterId;
        this.adminId = adminId;
    }

    public boolean isAcceptedByAdmin() {
        return acceptedByAdmin;
    }

    public void setAcceptedByAdmin(boolean acceptedByAdmin) {
        this.acceptedByAdmin = acceptedByAdmin;
    }

    public Integer getReporterId() {
        return reporterId;
    }

    public void setReporterId(Integer reporterId) {
        this.reporterId = reporterId;
    }

    public Integer getIdNews() {
        return idNews;
    }

    public void setIdNews(Integer idNews) {
        this.idNews = idNews;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}
