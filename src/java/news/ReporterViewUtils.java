package news;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIInput;
import javax.inject.Named;

/**
 *
 * @author radek
 */
@Named(value = "reporterViewUtils")
@RequestScoped
public class ReporterViewUtils implements Serializable {

    private UIInput titleInput;
    private UIInput textInput;
    private UIInput tagsInput;
    
    public ReporterViewUtils() {
    }
    
    public void clearAll() {
        titleInput.resetValue();
        textInput.resetValue();
        tagsInput.resetValue();
    }

    public UIInput getTitleInput() {
        return titleInput;
    }

    public void setTitleInput(UIInput titleInput) {
        this.titleInput = titleInput;
    }

    public UIInput getTextInput() {
        return textInput;
    }

    public void setTextInput(UIInput textInput) {
        this.textInput = textInput;
    }

    public UIInput getTagsInput() {
        return tagsInput;
    }

    public void setTagsInput(UIInput tagsInput) {
        this.tagsInput = tagsInput;
    }
    
}
