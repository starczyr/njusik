package news;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author radek
 */
@Named(value = "newsUtils")
@SessionScoped
public class NewsUtils implements Serializable {

    private NewsDAO newsDAO = new NewsDAO();

    public NewsUtils() {
    }

    public String getNewsStartText(News news) {
        if(news.getText().length() > 100) {
            return news.getText().substring(0, 100).concat("...");
        } else {
            return news.getText();

        }
    }

    public String getAuthorMail(Integer reporterId) {
        return "radek@gmail.com";
    }
}