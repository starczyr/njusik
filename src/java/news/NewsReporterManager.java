package news;

import exceptions.EntityAlreadyPersistedException;
import holders.AdminNewsHolder;
import java.io.Serializable;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import user.UserManager;

/**
 * Bean used in reporting view.
 *
 * @author radek
 */
@Named(value = "newsReporterManager")
@SessionScoped
public class NewsReporterManager implements Serializable {

    @Inject
    UserManager userManager;
    @Inject
    ReporterViewUtils reporterViewUtils;
    @Inject
    AdminNewsHolder adminNewsHolder;
    private String freshNewsTitle;
    private String freshNewsText;
    private String freshNewsTags;

    public NewsReporterManager() {
    }

    public void reset() {
        freshNewsTitle = "";
        freshNewsText = "";
        freshNewsTags = "";

        reporterViewUtils.clearAll();
    }

    public String reportFreshNews(String adressToRedirect) {
        if(FacesContext.getCurrentInstance().isValidationFailed()) {
            return adressToRedirect;
        }
        freshNewsTitle = Jsoup.clean(freshNewsTitle, Whitelist.basic());
        freshNewsText = Jsoup.clean(freshNewsText, Whitelist.basicWithImages());
        freshNewsTags = Jsoup.clean(freshNewsTags, Whitelist.simpleText());


        News news = new News(freshNewsTitle, freshNewsText, new Date(), freshNewsTags,
                userManager.getSessionManager().getIdUser(), null);

        NewsDAO newsDAO = new NewsDAO();
        try {
            newsDAO.save(news);
        } catch(EntityAlreadyPersistedException ex) {
            Logger.getLogger(NewsReporterManager.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Ups!", "Sorry, we encounter a problem. We're working on that."));
            return null;
        } catch(Exception e) {
            Logger.getLogger(NewsReporterManager.class.getName()).log(Level.SEVERE, null, e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Ups!", "Sorry, we encounter a problem. We're working on that."));
            return null;
        }

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                "Success!", "Your news has been reported!"));

        adminNewsHolder.updateSizes();

        return adressToRedirect;
    }

    public String getFreshNewsTitle() {
        return freshNewsTitle;
    }

    public void setFreshNewsTitle(String freshNewsTitle) {
        this.freshNewsTitle = freshNewsTitle;
    }

    public String getFreshNewsText() {
        return freshNewsText;
    }

    public void setFreshNewsText(String freshNewsText) {
        this.freshNewsText = freshNewsText;
    }

    public String getFreshNewsTags() {
        return freshNewsTags;
    }

    public void setFreshNewsTags(String freshNewsTags) {
        this.freshNewsTags = freshNewsTags;
    }
}
