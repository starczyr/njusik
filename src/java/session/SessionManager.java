package session;

import java.io.File;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

/**
 *
 * @author radek
 */
public class SessionManager {

    private static SessionManager sessionManager = new SessionManager();
    private String userMail;
    private boolean loggedIn;
    private String name;
    private String lastName;
    private String passwordRecoveryString;
    private boolean admin;
    private Integer idUser;
    private String status;
    private boolean blocked;
    private String pathToProfilePhoto;

    private SessionManager() {
    }

    public String getPathToProfilePhoto() {
        return getPathToImgFolder().concat("/main.jpg");
    }

    public void setPathToProfilePhoto(String pathToProfilePhoto) {
        this.pathToProfilePhoto = pathToProfilePhoto;
    }


    private String getPathToImgFolder() {
        Integer id = sessionManager.getIdUser();
        String path = ((ServletContext) FacesContext.getCurrentInstance()
                .getExternalContext().getContext()).getRealPath("/design/img/users");
        System.err.println("Path: " + path);
        
        
        File folderUsers = new File(path);
        
        String[] content = folderUsers.list();

        boolean alreadyContains = false;
        for(String string : content) {
            if(string.equals(id.toString())) {
                alreadyContains = true;
            }
        }
        
        File userUniqueFolder = new File(folderUsers, id.toString());
        
        if(!alreadyContains) {
            userUniqueFolder.mkdir();
        }
        String contextRelativePath = "/design/img/users";
        return contextRelativePath.concat("/").concat(userUniqueFolder.getName());
    }
    
    public static SessionManager getSessionManager() {
        return sessionManager;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getUserMail() {
        return userMail;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPasswordRecoveryString() {
        return passwordRecoveryString;
    }

    public void setPasswordRecoveryString(String passwordRecoveryString) {
        this.passwordRecoveryString = passwordRecoveryString;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void endSession() {
        this.userMail = null;
        this.name = null;
        this.lastName = null;
        this.loggedIn = false;
        this.admin = false;
    }
}
