package error;

/**
 * Handling error messages.
 * 
 * @author radek
 */
public class ErrorUtils {

    public enum Reason {

        TOO_MANY_TRIES, RECOVERY_STRING_NOT_FOUND, NOT_LOGGED_AS_ADMIN
    }
    private String[] mainMessages = {"Ups!", "Yay!", "Oh no!"};
    private String[] subMessages = {"Sorry, too many tries.",
        "It seams You didn't reset Your password.", "I think You'r not an admin!"};
    private static ErrorUtils errorUtils = new ErrorUtils();
    private static String errorMainMessage;
    private static String errorSubMessage;

    private ErrorUtils() {
    }

    public static ErrorUtils getErrorUtils() {
        return errorUtils;
    }

    public String getErrorSubMessage() {
        return errorSubMessage;
    }

    public String getErrorMainMessage() {
        return errorMainMessage;
    }

    public void setErrorMessages(Reason reason) {
        errorMainMessage = mainMessages[reason.ordinal()];
        errorSubMessage = subMessages[reason.ordinal()];

    }

    @Override
    public String toString() {
        return "ErrorUtils{" + "errorMessage=" + errorMainMessage + '}';
    }
}