package error;

import java.io.Serializable;
import java.util.Date;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import user.UserManager;

/**
 * Handling error massages and locking time.
 * @author radek
 */
@Named(value = "errorManager")
@SessionScoped
public class ErrorManager implements Serializable {

    private final int LOCKING_TIME = 5;
    @Inject
    UserManager userManager;
    ErrorUtils errorUtils = ErrorUtils.getErrorUtils();

    public ErrorManager() {
    }

    public int getMinutesToWait() {
        long whenBlocked = 0;
        try {
            whenBlocked = userManager.getWhenBlocked().getTime();
        } catch(Exception e) {
            System.err.println(e.toString());
        }
        long diff = new Date().getTime() - whenBlocked;

        return (int) (LOCKING_TIME - diff / 60000);
    }

    public ErrorUtils getErrorUtils() {
        return errorUtils;
    }
}
