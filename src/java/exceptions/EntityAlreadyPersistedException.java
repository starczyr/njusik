package exceptions;

/**
 * Specified entity is already in database.
 * 
 * @author radek
 */
public class EntityAlreadyPersistedException extends Exception {
    public EntityAlreadyPersistedException() {
    }
    
    public EntityAlreadyPersistedException(String message) {
        super(message);
    }
}
