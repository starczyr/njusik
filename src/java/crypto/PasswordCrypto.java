package crypto;

/**
 * Cryptographic class based on JBCrypt open source project.
 *
 * @author radek
 */
public class PasswordCrypto {

    /**
     * Encrypt password using 'hashpw' method from 'JBCypt' with gensalt = 12
     * 
     * @param password
     * @return encrypted password
     */
    public String encryptPassword(String password) {
	String hashed = BCrypt.hashpw(password, BCrypt.gensalt(12));
        
        return hashed;
    }
    
    /**
     * Matches candidate given as plain text with hashed password
     * @param hashed
     * @param candidate
     * @return result of equality test
     */
    public boolean passwordsEqual(String hashed, String candidate) {
        boolean result = BCrypt.checkpw(candidate, hashed);
        return result;
    }
}
